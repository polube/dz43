const express = require('express');
const postsRouter = require("posts.controller.js");
const es6Renderer = require("express-es6-template-engine");

const app = express();
app.engine("html", es6Renderer);
app.set("views", __dirname + "/views");
app.use(express.static("public"));
app.use(express.json());



app.post("/", (req, res) => {
    console.log(req.body)
    res.json({ message: "It`s post", body: req.body })
});

app.use("/posts", postsRouter);

app.listen(3000, () => {
    console.log("Server is up!");
})

app.get('/stuff', (req, res) => {
    res.status(404).send("Status 404");
})

app.get('/stuff/page', (req, res) => {
    res.send("This is stuff page!");
})

app.get('/stuff/data', (req, res) => {
    res.json({ data: "This is stuff data" });
})

app.post('/stuff/set', (req, res) => {
    if (req.body === {}) {
        res.status(400).send({ message: "No body passed!" });
    } else {
        res.json({ message: "OK" });
    }
    console.log(req.body);
})



